package edu.mondragon.webeng1.servlet_redirect.controller;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "RedirectController", urlPatterns = { "/redirect" })
public class RedirectController extends HttpServlet {
  private static final long serialVersionUID = 1L;
  String message;

  public RedirectController() {
    super();
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    message = request.getParameter("message");
    if (message == null || message.equals("")) {
      response.sendRedirect("error.html");
    } else {
      response.sendRedirect("ok.jsp");
    }
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }

}
