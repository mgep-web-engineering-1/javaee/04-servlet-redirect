package edu.mondragon.webeng1.servlet_redirect.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "ForwardController", urlPatterns = { "/forward" })
public class ForwardController extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private String message;

  public ForwardController() {
    super();
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    message = request.getParameter("message");
    if (message == null || message.equals("")) {
      response.sendRedirect("error.html");
    } else {
      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ok.jsp");
      dispatcher.forward(request, response);
    }
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }

}
