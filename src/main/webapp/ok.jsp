<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<%
	String message = request.getParameter("message");
	String tip = String.join("\n"
	    , "<div>"
	    , "<p>If redirect is used, 2 requests are made:</p>"
	    , "<ol>"
	    , "<li>Request to RedirectController (message parameter is on the request).</li>"
	    , "<li>Request to OK.jsp (as it is a new request, message parameter is lost).</li>"
	    , "</ol>"
	    , "<p>See also that URL changes, we have called <i>/redirect</i> and now we are at <i>/ok.jsp</i></p>"
	    , "</div>");
	if(message!=null){
		tip = String.join("\n"
		    , "<div>"
		    , "<p>If forwarding is used, only 1 request is made:</p>"
		    , "<ol>"
		    , "<li>From the form to ForwarderController, and the controller executes ok.jsp.</li>"
			, "</ol>"
			, "<p>See that message parameter has been printed correctly. Being a single request the parameter has not been lost.</p>"
		    , "<p>See also that the URL is <i>/forward</i> and not <i>/ok.jsp</i></p>"
		    , "</div>");
	}

%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>OK</title>
</head>

<body>
  <h1>Message: <%= message %></h1>
  <%= tip %>
  <p>Go back to <a href="/">main page</a>.</p>
</body>

</html>