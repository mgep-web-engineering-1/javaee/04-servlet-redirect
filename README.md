# Servlet Redirect (Redirect Vs Forward)

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Objective

* We have 2 forms:
  * (1) will call ```RedirectController```.
  * (2) will call ```ForwardController```.

* Both forms will give us the same result: ```ok.jsp```, but:
  * (1) will change the path of the action on the process.
  * (2) will maintain the path of the action (the one given in the form).

* If we access directly one of the paths of the controllers (```http://localhost:8080/forward``` or ```http://localhost:8080/redirect```) without any parameter, they will show ```error.jsp```.

## Explaination

* As we don't need any Java code in the main page, it is a simple HTML file instead of a JSP file.

* If we use the first form, we will make a request to ```/redirect```.
  * ```RedirectController``` will be called.
  * ```RedirectController``` will return a 302 (Redirect) response with the new URL
  * The browser will make a new request (```/ok.jsp```).
  * The server will process the JSP file and will return a 200 code and the HTML.
  * **message** parameter has been sent with the first request (```/redirect```).
    * As ```RedirectController``` did't make anything with the **message** parameter, the 2nd request (```/ok.jsp```) didn't have the parameter.
    * ```ok.jsp``` did not receive the parameter.

```java
// RedirectController.java
...
response.sendRedirect("ok.jsp");
...
```

* If we use the second form, we will make a request to ```/forward```.
  * ```ForwardController``` will be called.
  * ```ForwardController``` gets ```ok.jsp``` and processes it and return a 200 code with the resultant HTML.
  * As we are still in the first request (see that ```ok.jsp``` is not in the path), message parameter is still valid and we are able to print it.

```java
// ForwardController.java
...
RequestDispatcher dispatcher = getServletContext()
  .getRequestDispatcher("/ok.jsp");
dispatcher.forward(request, response);
...
```

* Finally, if we call ```/redirect``` or ```/forward``` directly without passing the **message** parameter, both will make a redirect to ```error.html```.

```java
// {Both}Controler.java
...
message = request.getParameter("message");
if(message==null || message.equals("")){
  response.sendRedirect("error.html");
}else{
  ...
```

**See the diference in the timing inspecting the network calls.**

## Test your understanding

* Create a controller to handle ```/error``` path and show content in ```error.html```.
  * **do not** make a second request in the process.
  * **do not** change the URL to ```error.html```.

## Next and before

* Before [03-first-servlet](https://gitlab.com/mgep-web-engineering-1/javaee/03-first-servlet)
* Next [05-redirect-with-parameters](https://gitlab.com/mgep-web-engineering-1/javaee/05-redirect-with-parameters)